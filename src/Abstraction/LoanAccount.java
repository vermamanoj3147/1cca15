package Abstraction;
//STEP 2
public class LoanAccount implements Account{
    private double loanAmount;
    //account creation
    public LoanAccount(double loanAmount) {
        this.loanAmount = loanAmount;
        System.out.println("Loan Account Created");
    }

    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+" Rs debited from your loan acc");

    }

    @Override
    public void withdraw(double amt) {
        loanAmount+=amt;
        System.out.println(amt+" Rs credited to your loan acc");

    }

    @Override
    public void checkBalance() {
        System.out.println("ACTIVE LOAN AMOUNT" + loanAmount);

    }
}
