package Abstraction;

public class SavingsAccount implements Account {
   private double accountBalance;
    //account creation
    public SavingsAccount(double accountBalance) {
        this.accountBalance= accountBalance;
        System.out.println("Savings Account Created");
    }

    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+" Rs credited to your acc");

    }

    @Override
    public void withdraw(double amt) {
        if (amt<=accountBalance){
            accountBalance-=amt;
            System.out.println(amt+ " Rs debited from your acc");
        }else {
            System.out.println("Insufficient account balance");
        }

    }

    @Override
    public void checkBalance() {
        System.out.println("Active Balance "+accountBalance);
    }
}
