package PatternTest;

public class PatternTest5 {
    public static void main(String[] args) {
        int line=5;
        int star=1;
        int space=4;
        int ch=1;
        for (int i=0;i<line;i++)
        {
            //int ch1=ch;
            for (int k=0;k<space;k++)
            {
                System.out.print(" ");
            }
            for (int j=0;j<star;j++)
            {
                if (ch>6)
                    ch=1;
                if(j%2==0 ){
                System.out.print(ch++);}
                else
                    System.out.print(" ");
            }
            System.out.println();
            star+=2;
            space--;
        }
    }
}
