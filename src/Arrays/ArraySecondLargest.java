package Arrays;

public class ArraySecondLargest {
    public static void main(String[] args) {
        int [] arr={2,10,4,2,1,7,7,3,5};
        int max1=arr[0];
        int max2=arr[1];
        for(int a:arr)
        {
            if(a>max1){
                max2=max1;
            max1=a;}
           else if (a>max2 && a!=max1)//And condition is used if duplicate largest number exists
                max2=a;

        }
        System.out.println("MAX1: "+max1);
        System.out.println("MAX2: "+max2);
    }
}
