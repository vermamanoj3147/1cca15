package Arrays;

public class ArrayLargestNumber {
    public static void main(String[] args) {
        int [] arr={1,2,3,4,5};
        int max=arr[0];
        for(int a:arr)//for(int i=0;i<arr.length;i++)
        {
            if(a>max)//if(arr[i]>max)
                max=a;//max=arr[i];
        }
        System.out.println(max);
    }
}
