package Pattern;

public class Pattern46 {
    public static void main(String[] args) {
        int line=5;
        int star=1;
        for(int i=0;i<line;i++)
        {
            int ch2 = 0;
            for(int j=0;j<star;j++) {
                if (j == 2) {
                    System.out.print(1);
                } else {
                    System.out.print(ch2++);
                }
            }
            System.out.println();
            star++;
        }
    }
}
