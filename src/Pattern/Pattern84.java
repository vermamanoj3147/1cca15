package Pattern;

public class Pattern84 {
    public static void main(String[] args) {
        int line=4;
        int star=1;
        int space=4;
        int ch=0;
        for(int i=0;i<line;i++)
        {
            int ch1=ch;
            for (int k=0;k<space;k++)
            {
                System.out.print(" ");
            }
            for (int j=0;j<star;j++)
            {
                if(i>j)
                System.out.print(ch1--);
                else
                    System.out.print(ch1++);
            }
            System.out.println();
            ch++;
            star+=2;
            space--;
        }
    }
}
