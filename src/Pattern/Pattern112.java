package Pattern;

public class Pattern112 {
    public static void main(String[] args) {
        int line=10;
        int star=10;
        int space=0;
        int ch=1;
        for (int i=0;i<line;i++)
        {
            int ch1=ch;
            for (int k = 0; k < space; k++)
            {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++)
            {
                if (j%2==0)
                System.out.print(" ");
                else
                    System.out.print(ch1++);
            }
                System.out.println();
                if (i<=3){
                    ch++;
                    star-=2;
                    space++;
                } else if (i>=5) {
                    ch--;
                    star+=2;
                    space--;

                }
            }
        }
    }
