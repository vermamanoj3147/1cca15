package Pattern;

public class Pattern39 {
    public static void main(String[] args) {
        int line=10;
        int star=1;

        for (int i=0;i<line;i++)
        {
            int ch=1;
            for( int j=0;j<star;j++)
            {
                System.out.print(ch++*star+ "  ");
            }
            System.out.println();
            star++;
        }
    }
}
