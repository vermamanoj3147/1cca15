package Pattern;

public class Pattern91 {
    public static void main(String[] args) {
        int line=9;
        int star=5;
        //int space=0;
        for (int i=0;i<line;i++)
        {
            for (int j=0;j<star;j++)
            {
                System.out.print("*");

            }
            System.out.println();
            if(i<=3)
            {
               star--;
            }
            else{
                star++;

            }
        }
    }
}
