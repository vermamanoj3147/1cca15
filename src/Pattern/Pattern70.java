package Pattern;



public class Pattern70 {
    public static void main(String[] args) {
        int line =5;
        int star=1;
        int space=4;
        int ch=1;
        for(int i=0;i<line;i++)
        {
            for(int k=0;k<space;k++)
            {
                System.out.print(" ");
            }
            for(int j=0;j<star;j++)
            {
                if(i==j)
                {
                    System.out.print(ch);
                }
                else{
                    System.out.print("*");
                }
            }
            System.out.println();
            star+=2;
            space--;
            ch++;

        }

    }
}
