package Pattern;

public class Pattern101 {
    public static void main(String[] args) {
        int line =12;
        int star=6;
        char ch='F';
        for (int i=0;i<line;i++) {
            char ch1=ch;
            for (int j = 0; j < star; j++) {
                System.out.print(ch1--);
            }
            System.out.println();
            if(i<=4)
            {
                star--;
                ch--;
            } else if (i>=6) {
                star++;
                ch++;
            }
        }

    }
}
