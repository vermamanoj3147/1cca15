package Pattern;

public class Pattern12 {
    public static void main(String[] args) {
        int line=4;
        int star=5;
        for(int i=0; i<line; i++)
        {
            int ch = 5;
            for (int j = 0; j < star; j++)
            {
                System.out.print(ch);
                ch--;
            }
            System.out.println();
        }
    }
}
