package Pattern;

public class Pattern58 {
    public static void main(String[] args) {
        int line=9;
        int star=1;
        for(int i=0;i<line;i++)
        {
            int ch=1;
            for(int j=0;j<star;j++)
            {
                if(j%2==0)
                {
                    System.out.print(ch);
                }
                else {
                    System.out.print(0);
                }

            }
            System.out.println();
            star++;
        }
    }
}
