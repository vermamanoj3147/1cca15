package Pattern;

public class Pattern79 {
    public static void main(String[] args) {
        int line =9;
        int star=9;
        int space=0;
        for(int i=0;i<line;i++)
        {
            for(int k=0;k<space;k++)
            {
                System.out.print(" ");
            }
            for (int j=0;j<star;j++)
            {
                if(j%2==0)
                System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println();
            if (i<=3)
            {
                star-=2;
                space++;
            }
            else {
                star += 2;
                space--;
            }
        }
    }
}
