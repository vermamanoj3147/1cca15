package Pattern;

public class Pattern16 {
    public static void main(String[] args) {
        int line = 3;
        int star = 4;
        int ch1 = 1;                               //1234
                                                  //3456
                                                  //5678
        for (int i = 0; i < line; i++)
        {
            int ch2=ch1;
            for(int j=0;j<star;j++)
            {
                System.out.print(ch2++);
            }
            System.out.println();
            ch1+=2;

        }
    }
}
