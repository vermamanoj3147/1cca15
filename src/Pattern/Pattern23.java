package Pattern;

public class Pattern23 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        //int star=1;

        for(int i=0;i<line;i++)
        {
            for(int j=0;j<star;j++)
            {
                if(i>=j)
                    System.out.print(" * ");

            }
            System.out.println();
            star++;
        }
    }
}
