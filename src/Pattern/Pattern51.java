package Pattern;

public class Pattern51 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int ch1 = 1;
        for (int i = 0; i < line; i++) {
            int ch2=ch1;
            for (int j = 0; j < star; j++) {
                if (i == j) {
                    ch2 = j + 1;
                    System.out.print(ch2+"  ");
                } else {
                    System.out.print(ch1+"  ");
                }
            }
            System.out.println();
        }
    }
}
