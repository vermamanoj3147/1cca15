package Pattern;

public class Pattern34 {
    public static void main(String[] args) {
        int line=9;
        int star=1;
        char ch='A';

        for(int i=0;i<line;i++)
        {
            for(int j=0; j<star; j++)
            {
                System.out.print(ch++);
                if(ch>'E')
                    ch='A';
            }
            System.out.println();
            if(i<4)
                star++;
            else
                star--;
        }
    }
}
