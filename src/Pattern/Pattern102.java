package Pattern;

public class Pattern102 {
    public static void main(String[] args) {
        int line=6;
        int star=1;
        int space=5;

        for (int i=0;i<line;i++){
            char ch='A';
            for (int k=0;k<space;k++)
            {
                System.out.print(" ");
            }
            for (int j=0;j<star;j++)
            {
                if (j%2==0)
                System.out.print(ch++);
                else System.out.print(" ");
            }
            System.out.println();
            star+=2;space--;
        }
    }
}
