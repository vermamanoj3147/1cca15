package Pattern;

public class Number1 {
    public static void main(String[] args) {
        //to find count of digits inside a number,eg:- count of zero inside the given number.
        long a=1110001021301l;
        int count=0;
        while(a!=0)
        {
            long r=a%10;
            if (r==0)
                count++;
            a=a/10;
        }
        System.out.println(count);
    }
}
