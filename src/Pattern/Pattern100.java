package Pattern;

public class Pattern100 {
    public static void main(String[] args) {
        int line=12;
        int star=6;
        for (int i=0;i<line;i++)
        {
            char ch='A';
            for (int j=0;j<star;j++)
            {
                System.out.print(ch++);
            }
            System.out.println();
            if (i<=4){
                star--;
            }else if (i>=6){
                star++;
            }
        }
    }
}
