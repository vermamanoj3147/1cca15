package Pattern;

public class Pattern33 {
    public static void main(String[] args) {
        int line=5;
        int star=1;
        char ch='A';
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<star;j++)
            {
                System.out.print(ch);
            }
            System.out.println();
            ch+=2;
            if(i<2)
                star++;
            else
                star--;
        }
    }
}
