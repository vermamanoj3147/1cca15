package Pattern;

public class Pattern59 {
    public static void main(String[] args) {
        int line = 9;
        int star = 9;
        int ch=1;
        for (int i = 0; i < line; i++)
        {

            for(int j=0;j<star;j++)
            {
                if(j%2!=0)
                {
                    System.out.print(0+ " ");
                }
                else {
                    System.out.print(ch+ " ");
                }
            }
            System.out.println();
            if(i%2==0)
            {
                System.out.print(0+ " ");
            }
            else {
                System.out.print(ch+ " ");
            }
        }
    }
}
