package Pattern;

public class Pattern22 {
    public static void main(String[] args) {
        int line=5;
        int star=5;

        for(int i=0; i<line;i++)
        {
            int ch1=1;
            char ch2='A';
            for(int j=0;j<star;j++)
            {
                if(i%2==0) {
                    System.out.print(ch1+" ");
                    ch1++;
                }
                else
                {
                    System.out.print(ch2+" ");
                ch2++;
                }
            }
            System.out.println();

        }
    }
}
